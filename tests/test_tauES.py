import os.path
import numpy as np
import pytest
import shutil


pytestmark = pytest.mark.skipif(shutil.which("correction") is None, reason="CorrectionLib was not found")


testData = os.path.join(os.path.dirname(__file__), "data")


@pytest.fixture(scope="module")
def nanotauargs():
    from bamboo.root import gbl
    f = gbl.TFile.Open(os.path.join(testData, "DY_M50_2016.root"))
    tup = f.Get("Events")
    tup.GetEntry(0)
    i = 0
    while tup.nTau < 2:
        i += 1
        tup.GetEntry(i)
    RVec_float = gbl.ROOT.VecOps.RVec["float"]
    RVec_int = gbl.ROOT.VecOps.RVec["Int_t"]
    tau_pt = RVec_float(tup.Tau_pt, tup.nTau)
    tau_mass = RVec_float(tup.Tau_mass, tup.nTau)
    tau_eta = RVec_float(tup.Tau_eta, tup.nTau)
    tau_decayMode = RVec_int(tup.Tau_decayMode, tup.nTau)
    tau_genPartFlavAsInt = np.array(tup.Tau_genPartFlav, dtype="int32")
    tau_genPartFlav = RVec_int(tau_genPartFlavAsInt, tup.nTau)
    yield (tau_pt, tau_mass, tau_eta, tau_decayMode, tau_genPartFlav, "DeepTau2017v2p1")


@pytest.fixture(scope="module")
def tauEScalc_2016():
    from bamboo.root import gbl, loadcorrectionlib, loadTauESCorrectionCalculator
    loadcorrectionlib()
    loadTauESCorrectionCalculator()
    calc = gbl.TauESCorrectionCalculator(os.path.join(testData, "tau2016.json.gz"))
    yield calc


def test_tauES_nano_mc(tauEScalc_2016, nanotauargs):
    nanotau = nanotauargs
    res_mc = tauEScalc_2016.produce(*nanotau)
    assert res_mc
